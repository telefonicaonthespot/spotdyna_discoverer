program discoverer;

uses
  Vcl.Forms,
  broadcast in 'broadcast.pas' {main},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  TStyleManager.TrySetStyle('Windows10 BlackPearl');
  Application.CreateForm(Tmain, main);
  Application.Run;
end.
