unit broadcast;

////////////////////////SPOTDYNA DISCOVERER/////////////////
///   V1.0.0.1   - Version inicial.
///   V1.0.0.2   - Quitamos la URL de forzado y a�adir nom_ubicacion de la config.
///   V1.0.0.4   - Doble timeout
interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, IdBaseComponent,
  IdComponent, IdUDPBase, IdUDPClient, IdCustomTCPServer, IdCustomHTTPServer,
  IdHTTPServer, Vcl.ComCtrls, shellapi, system.JSON,TLHelp32, IdContext,
  IdUDPServer, IdGlobal, IdSocketHandle, IdTCPConnection, IdTCPClient, IdHTTP,OXmlPDOM,
  Vcl.ExtCtrls, AdvOfficeImage, BMDThread, clHttpRequest, clTcpClient,
  clTcpClientTls, clHttp, FormSize, dcMinTray, JvComponentBase, JvTrayIcon;

type
  Tmain = class(TForm)
    udp1: TIdUDPClient;
    Button1: TButton;
    server1: TIdHTTPServer;
    UDPServer: TIdUDPServer;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Splitter2: TSplitter;
    lista: TListBox;
    AdvOfficeImage1: TAdvOfficeImage;
    Label1: TLabel;
    Timer1: TTimer;
    Http1: TclHttp;
    FormSize1: TFormSize;
    DCMinTray1: TDCMinTray;
    AdvOfficeImage2: TAdvOfficeImage;
    JvTrayIcon1: TJvTrayIcon;
    mtty: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure server1CommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure server1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure UDPServerUDPRead(AThread: TIdUDPListenerThread;
      const AData: TIdBytes; ABinding: TIdSocketHandle);
    procedure Timer1Timer(Sender: TObject);
    procedure mTTYDblClick(Sender: TObject);
    procedure listaDblClick(Sender: TObject);
    procedure mttyChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    m_debug : boolean;
    procesando: boolean;
    Procedure TTY(S : String; tipo_log:integer=1);
    procedure myexceptionhandler(Sender: TObject; E: Exception);
    procedure reiniciar_aplicacion;
    procedure matar_proceso(proceso: string);
  end;

var
  main: Tmain;
  logerror  : TextFile;
  aSnapshotHandle     : THandle;
  aProcessEntry32     : TProcessEntry32;

implementation

{$R *.dfm}

function GetAppVersionStr: string;
var
  Exe: string;
  Size, Handle: DWORD;
  Buffer: TBytes;
  FixedPtr: PVSFixedFileInfo;
begin
  Exe := ParamStr(0);
  Size := GetFileVersionInfoSize(PChar(Exe), Handle);
  if Size = 0 then
    RaiseLastOSError;
  SetLength(Buffer, Size);
  if not GetFileVersionInfo(PChar(Exe), Handle, Size, Buffer) then
    RaiseLastOSError;
  if not VerQueryValue(Buffer, '\', Pointer(FixedPtr), Size) then
    RaiseLastOSError;
  Result := Format('%d.%d.%d.%d',
    [LongRec(FixedPtr.dwFileVersionMS).Hi,  //major
     LongRec(FixedPtr.dwFileVersionMS).Lo,  //minor
     LongRec(FixedPtr.dwFileVersionLS).Hi,  //release
     LongRec(FixedPtr.dwFileVersionLS).Lo]) //build

   //Result:=_VERSION;
end;

procedure Tmain.Button1Click(Sender: TObject);
begin
    udp1.Broadcast('DISCOVER',6776);
end;

procedure Tmain.Timer1Timer(Sender: TObject);
begin
    timer1.Enabled:=false;
end;

Procedure TMain.TTY(S : String; tipo_log:integer=1);
var
   nombrefic :  string;
begin
  try
       //enviar_mensaje_ws(s);
       if mTTY.lines.count > 100 then mTTY.Lines.Delete(0);
//       case tipo_log of
//            0: mTTY.SelAttributes.Color:=clsilver;   //Informativo
//            1: mTTY.SelAttributes.Color:=clwhite;  //General
//            2: mTTY.SelAttributes.Color:=clWebCyan;   //Proceso
//            3: begin
//                  mTTY.SelAttributes.Color:=clWebOrange;    //Error
//                  mTTY.SelAttributes.Style:=mTTY.SelAttributes.Style+[fsbold];
//               end;
//            4: mTTY.SelAttributes.Color:=clYellow;  //Loguear en el fichero de log
//       end;

       mTTY.lines.add(DateTimeToStr(now)+' - '+s);

       if (tipo_log=3) OR (tipo_log=4) then
        begin
            s:=stringreplace(s,Chr(13),' ',[rfReplaceAll]);
            s:=stringreplace(s,Chr(10),'',[rfReplaceAll]);
            nombrefic := extractfilepath(application.ExeName)+'\logs\LOG_discoverer'+formatdatetime('yyyymmdd',now)+'.LOG';
            //Escritura en el log de errores para la monitorizacion
            try
                AssignFile(logerror, nombrefic);
                if not FileExists(nombrefic) then
                    Rewrite(logerror)
                    else
                        Append(logerror);
                Writeln(logerror,FormatDateTime('dd/mm/yyyy hh:nn:ss',now)+' - '+s);
                CloseFile(logerror);
            except

            end;
            //Lo a�adimos en el memo para que envie el mail
        end;
  except
    on e:exception do
    begin
        mTTY.Lines.Add(FormatDateTime('dd/mm/yyyy hh:nn:ss',now)+' - ERROR EN TTY... NO PUEDO AUTOESCRIBIRME EL LOG.');
    end;

  end;
end;


procedure Tmain.reiniciar_aplicacion;
var
    comando     ,
    parametros  : string;
begin
    comando:='"'+application.exename+'"';
    parametros:='';
    ShellExecute(handle,'open',PWideChar(comando), PWideChar(parametros),nil,SW_SHOWNORMAL);

end;

procedure Tmain.matar_proceso(proceso: string);
var
  Ret       ,
  bContinue : BOOL;
  PrID      ,
  PrID_este : Integer; // processidentifier
  Ph        : THandle;   // processhandle
begin
     try
        aSnapshotHandle        := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
        aProcessEntry32.dwSize := SizeOf(aProcessEntry32);
        bContinue := Process32First(aSnapshotHandle, aProcessEntry32);
        while Integer(bContinue) <> 0 do
        begin
                PrID:=StrToInt('$' + IntToHex(aProcessEntry32.th32ProcessID, 4));
                if aProcessEntry32.szExeFile=proceso then
                    begin
                        Ph := OpenProcess(1, BOOL(0), PrID);
                        PrID_este:=GetCurrentProcessId;
                        if PrID_este<>PrID then
                            begin
                                Ret := TerminateProcess(Ph, 0);
                                if Integer(Ret) = 0
                                   then tty('No se ha podico matar ' + proceso,3)
                                   else tty('Proceso ' + proceso + ' matado');
                            end;
                    end;
                bContinue := Process32Next(aSnapshotHandle, aProcessEntry32);
         end;
         CloseHandle(aSnapshotHandle);
     Except
       on e:Exception do
          begin
            tty('Error Cerrando procesos previos: ' + e.message,3);
          end;

     end;
end;


procedure Tmain.mttyChange(Sender: TObject);
begin
    SendMessage(mtty.handle, WM_VSCROLL, SB_BOTTOM, 0);
end;

procedure TMain.myexceptionhandler(Sender: TObject; E: Exception);
begin
    //TTY('(myexceptionhandler) - Begin');
    //Si existe el fichero c:\debug.txt, saltar� la excepci�n y parar� el programa
    if fileexists('c:\debug.txt') then
        raise E
    else
    begin
       //si no existe, se mostrar� en el cuadro de debug y proseguir� el proceso.
       tty('Detectada excepcion no controlada provocada por ' + TControl(Sender).Name+': '+E.message,3);
    end;
end;

procedure Tmain.server1CommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
    comando         : string;
    ip              ,
    respuesta       ,
    des_cunia       ,
    tags            ,
    fichero         ,
    cadena          ,
    ubicacion       : string;
    cod_terminal    ,
    tam_lista       ,
    n               : integer;

    parrilla        ,
    arrsalida       : Tjsonarray;
    item            : tjsonobject;
    cunia           : tjsonobject;
    XML             : IXMLDocument;
    Root            ,
    node            ,
    Attribute       ,
    xmlcunia        : PXMLNode;

    response        : TStrings;

begin
    response:=TStringList.Create();

    comando:=stringreplace(ARequestInfo.Document,'/','',[rfreplaceAll]);
    if m_debug then tty('recibido request');

    if comando='list' then
        begin
             lista.items.Clear;
             udp1.Broadcast('DISCOVER',6776);
             timer1.Enabled:=true;
             if m_debug then tty('list');
             tam_lista:=-1;
             while tam_lista<>lista.items.count do
                begin
                    tam_lista:=lista.items.count;
                    sleep(250);
                end;
             arrsalida:= TJSONArray.create();
             for n := 0 to lista.items.count-1 do
                try
                    cadena:=lista.items[n];
                    IP           := Copy(Cadena,1, pos('#',Cadena) - 1);
                    delete(cadena,1,pos('#',Cadena));
                    COD_TERMINAL := StrToInt(Copy(Cadena,1, pos('#',Cadena) - 1));
                    item :=TJSONObject.Create;
                    item.AddPair(TJSONPair.Create('id',TJSONNumber.Create(cod_terminal)));
                    item.AddPair(TJSONPair.Create('ip',ip));
                    item.AddPair(TJSONPair.Create('api','http://'+ip+':55555'));
                    tty('Procesar terminal '+inttostr(cod_terminal));
                    //Primero sacar nom_ubicacion de la config
                    Http1.Close;
                    if m_debug then tty('Enviando peticion API config',0);
                    http1.Get('http://'+ip+':55555/Getconfig?section=CONFIGURACION&key=nom_ubicacion',response);
                    respuesta:=response.Text;
                    if m_debug then tty('Respuesta :'+respuesta,0);
                    XML := CreateXMLDoc;
                    if m_debug then tty('Parse XML config :',0);
                    xml.LoadFromXML(respuesta);
                    Root := XML.DocumentElement;
                    Root.findchild('value',node);
                    ubicacion:=node.Text;
                    xml.Clear;
                    item.AddPair(TJSONPair.Create('ubicacion',ubicacion));
                    if m_debug then tty('FIN XML config :',0);

                    sleep(1000);
                    if m_debug then tty('Enviando peticion API parrilla',0);
                    //Ahora a sacar la parrilla
                    Http1.Close;
                    http1.Get('http://'+ip+':55555/getmediaplaylist',response);
                    respuesta:=response.Text;
                    if m_debug then tty('Respuesta :'+respuesta,0);


                    XML := CreateXMLDoc;
                    if m_debug then tty('Parse XML parrilla :',0);
                    xml.LoadFromXML(respuesta);
                    Root := XML.DocumentElement;
                    xmlcunia := nil;
                    parrilla:=TJSONArray.create();
                    while Root.GetNextChild(xmlcunia) do
                        begin
                            xmlcunia.findchild('DES_CUNIA',node);
                            des_cunia:=node.text;
                            xmlcunia.findchild('TAGS',node);
                            tags:=node.text;
                            xmlcunia.findchild('FIC_MULTIMEDIA',node);
                            fichero:=node.text;
                            cunia :=TJSONObject.Create;
                            cunia.AddPair(TJSONPair.Create('desc',des_cunia));
                            cunia.AddPair(TJSONPair.Create('tags',tags));
                            cunia.AddPair(TJSONPair.Create('fichero',fichero));
                            parrilla.AddElement(cunia);
                        end;
                    item.AddPair(TJSONPair.Create('playlist',parrilla));
                    //lista.items.add(item.ToString);
                    arrsalida.AddElement(item);
                    if m_debug then tty('Fin procesado parrilla :',0);
                    xml.Clear;
                except
                    on e:exception do
                    begin
                        tty('Error obteniendo datos de terminal '+e.Message);
                        tty(lista.items[n]);
                    end;
                end;

             tty('Devolviendo respuesta :');
             Aresponseinfo.ContentType:='application/json';
             Aresponseinfo.CustomHeaders.Clear;
             Aresponseinfo.CustomHeaders.AddValue('Access-Control-Allow-Origin','*');
             AResponseInfo.ContentText :=arrsalida.ToString;



        end;

end;


procedure Tmain.server1Status(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
    tty( AStatusText);
end;


procedure Tmain.mTTYDblClick(Sender: TObject);
begin
    mtty.Lines.Clear;
end;

procedure Tmain.FormCreate(Sender: TObject);
begin
    m_debug:=fileexists('c:\debug.txt');
    matar_proceso(extractfilename(application.ExeName));
    self.caption:=self.caption+' '+GetAppVersionStr;
    application.OnException := myexceptionhandler;

    //Arranca minimizada si no esta en debug
    if not m_debug then
      begin
         Self.WindowState:=wsMinimized;
         application.Minimize;
         self.SendToBack;
      end else begin
         Self.WindowState:=wsNormal;
         application.Restore;
      end;

    server1.Active:=true;
    udpServer.Active:=true;

end;

procedure Tmain.listaDblClick(Sender: TObject);
begin
    lista.items.Clear;
end;

procedure Tmain.UDPServerUDPRead(AThread: TIdUDPListenerThread;
  const AData: TIdBytes; ABinding: TIdSocketHandle);
var
    cadena          : string;

begin
    cadena:=BytesToString(aData,0,500);
    if m_debug then tty(cadena);
    lista.items.add(ABinding.PeerIP+'#'+cadena);
end;

end.
